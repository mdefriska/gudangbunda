import React, {useState,useEffect} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, Button } from 'react-native';
import {dataRef} from './Reference';

export default function Tambah({navigation,route}) {
    let [namaBarang, setNamabarang] = useState("");
    let [key, setKey] = useState("");
    let [jumlah, setJumlah] = useState("");
    let [dbarang, setDBarang] = useState([]);

    

     const submit = () => {
         let newData={
             barang: namaBarang,
             jumlah: jumlah
        };
        const ref = dataRef.push(newData);
        const key = ref.key;
        dataRef.child(key).update({'key': key})
        navigation.navigate('Home')
    };

    return (
        <View style={styles.container}>
            <View style={styles.titleBarang}>
                <Text style={styles.textTitle}>Tambah Barang</Text>
            </View>
            <View style={{backgroundColor:'#FFB894', paddingBottom:30, marginLeft:15, marginRight:15}}>
                <View style={styles.form}>
                    <View style={styles.itemForm}>
                        <TextInput
                            style={{paddingLeft:20, width: 230}}
                            placeholder='Nama Barang'
                            placeholderTextColor='#FF5500'
                            value={namaBarang}
                            onChangeText={(value)=>setNamabarang(value)}
                        />
                    </View>
                    <View style={styles.itemForm}>
                        <TextInput
                            style={{paddingLeft:20, width: 230}}
                            placeholder='Jumlah'
                            placeholderTextColor='#FF5500'
                            value={jumlah}
                            onChangeText={(value)=>setJumlah(value)}
                        />
                    </View>
                    <View style={styles.buttonView}>
                        <TouchableOpacity
                          style={styles.button}
                          onPress={()=>submit()}
                        >
                    <Text style={styles.buttonText}>Tambahkan</Text>
                </TouchableOpacity>
            </View>
                </View>
                </View>
		    </View>
    )
  }

const styles = StyleSheet.create({
	container:{
        flex:1,
    },
    titleBarang:{
        marginTop: 40,
        marginLeft:25,
        marginBottom: 25
    },
    textTitle:{
        fontWeight:'bold',
        fontSize:25,
        color:'#FF5500'
    },
    form:{
        marginTop:20
    },
    itemForm:{
        flexDirection: 'row',
        borderBottomWidth:1,
        borderBottomColor:'#FF5500',
        marginTop: 20,
        marginLeft : 30,
        marginRight : 30,
        marginBottom : 15
    },
    buttonView:{
        marginLeft:30,
        marginRight:30,
        marginTop: 55,
        alignItems:'center'
    },
    button:{
        backgroundColor:'#FF5500',
        height:35,
        width: 250,
        borderRadius:7,
        alignItems: 'center',
        justifyContent:'center',
    },
    buttonText:{
        color: 'white',
        fontWeight:'bold',
        fontSize:17,   
    },
})