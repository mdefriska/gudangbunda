import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import File from './File/index';

export default function App() {
  return (
    <File /> 
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
