import React, {useState} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, Button } from 'react-native';
import { FontAwesome} from '@expo/vector-icons';
import 'react-native-gesture-handler';

export default function Login ({navigation}) {
    let [username, setUsername] = useState("");
    let [password, setPassword] = useState("");
    let [isError, setIsError] = useState(false);
   
    const submit = (password) => {
        if(password=='1234'){
            navigation.navigate("MyDrawer",{
                Screen: 'Home'
            })
        }
        else{ isError=true}
        
      
    };
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.logo}>
                        <Image
                            source={require('./image/logo.jpg')}
                            style={{height:150, width:150}}
                        />
                    </View>
                    <View style={styles.title}>
                            <Text style={styles.titleText}>Sign In</Text>
                    </View>
                </View>
                <View style={styles.form}>
                    <View style={styles.itemForm}>
                        <FontAwesome name="user-circle" size={20} color='#FF5500' />
                        <TextInput
                            style={{paddingLeft:20, width: 230}}
                            placeholder='Username'
                            placeholderTextColor='#FF5500'
                            value={username}
                            onChangeText={(value)=>setUsername(value)}
                        />
                    </View>
                    <View style={styles.itemForm}>
                        <FontAwesome name="lock" size={28} color='#FF5500' />
                        <TextInput
                            style={{paddingLeft:20, width: 230}}
                            placeholder='Password'
                            placeholderTextColor='#FF5500'
                            value={password}
                            onChangeText={(value)=>setPassword(value)}
                        />
                    </View>
                    <View style={styles.buttonView}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={()=>submit(password)}
                        >
                            <Text style={styles.buttonText}>Sign In</Text>
                        </TouchableOpacity>
                    </View>
                </View>
		    </View>
    )
  }

const styles = StyleSheet.create({
	container:{
		flex:1,
        backgroundColor: 'white',
        flexDirection:'column'
	},
	header:{
		backgroundColor:'white',
		marginTop:100,
        flexDirection: 'column',
	},
	logo:{
        alignItems: 'center',
        justifyContent:'center',
	},
    title:{
        justifyContent:'center',
        marginLeft:33,
        marginTop:50
    },
    titleText:{
        color:'#FF5500', 
        fontSize:25, 
        fontWeight: 'bold',
    },
    form:{
        marginTop:20
    },
    itemForm:{
        flexDirection: 'row',
        borderBottomWidth:1,
        borderBottomColor:'#FF5500',
        marginTop: 20,
        marginLeft : 30,
        marginRight : 30,
        marginBottom : 15
    },
    buttonView:{
        marginLeft:30,
        marginRight:30,
        marginTop: 55
    },
    button:{
        backgroundColor:'#FF5500',
        height:35,
        width: 300,
        borderRadius:7,
        alignItems: 'center',
        justifyContent:'center',
    },
    buttonText:{
        color: 'white',
        fontWeight:'bold',
        fontSize:17,   
    },
    signup:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center',
        marginTop:60
    },
})