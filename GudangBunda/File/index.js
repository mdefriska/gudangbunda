import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator, useIsDrawerOpen } from '@react-navigation/drawer';
import 'react-native-gesture-handler';

import Login from './Login';
import Barang from './Barang';
import Tambah from './Tambah';
import Ubah from './Ubah';
import About from './About';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function index() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='LoginScreen' component={Login} options={{ headerShown: false }}/>
                <Stack.Screen name='MyDrawer' component={MyDrawer} options={{ headerShown: false }}/>
                <Stack.Screen name='MainApp' component={MainApp} options={{ headerShown: false }}/>
                <Stack.Screen name='MyStack' component={MyStack} options={{ headerShown: false }}/>
                
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp=()=>{
    return(
        <Tab.Navigator
          tabBarOptions={{
            activeTintColor:'#FF5500',
            inactiveTintColor: 'white',
            activeBackgroundColor: '#FFB894',
            labelStyle:{fontSize:15, fontWeight:'bold', marginBottom:10},
            style:{
                backgroundColor: '#FF5500',
                height:40
            }
            }}
        >
            <Tab.Screen name='Home' component={Barang}/>
            <Tab.Screen name='Tambah' component={MyStack} />
        </Tab.Navigator>
    )
}
const MyStack=()=>{
    return(
    <Stack.Navigator>
        <Stack.Screen name='Tambah' component={Tambah} options={{ headerShown: false }}/>
        <Stack.Screen name='Ubah' component={Ubah} options={{ title:'Kembali' }}/>
  </Stack.Navigator>
    )
}

const MyDrawer=()=>{
    return(
        <Drawer.Navigator>
            <Drawer.Screen name="Home" component={MainApp} />
            <Drawer.Screen name='About' component={About} />
        </Drawer.Navigator>
)}