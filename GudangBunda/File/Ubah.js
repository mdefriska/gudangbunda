import React, {useState,useEffect} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, Button } from 'react-native';
import {dataRef} from './Reference';

export default function Ubah({navigation,route}) {
    let {key} = route.params;
    let {namaBarang} = route.params;
    let {jumlah} = route.params;  
    let [namBar, setNamabarang] = useState(namaBarang);
    let [keyUbah, setKey] = useState(key);
    let [jumBar, setJumlah] = useState(jumlah);
    
     const ubahData = (keyUbah) => {
        dataRef.child(keyUbah).update({'barang': namBar,
                                       'jumlah' : jumBar});
        navigation.navigate('Home');
    };

    const hapusData = (keyUbah) =>{
        dataRef.child(key).remove();
        navigation.navigate('Home');
     }

    return (
        <View style={styles.container}>
            <View style={styles.titleBarang}>
                <Text style={styles.textTitle}>Ubah Data Barang</Text>
            </View>
            <View style={{backgroundColor:'#FFB894', paddingBottom:30, marginLeft:15, marginRight:15}}>
                <View style={styles.form}>
                    <View style={styles.itemForm}>
                        <TextInput
                            style={{paddingLeft:20, width: 230}}
                            value={namBar}
                            onChangeText={(value)=>setNamabarang(value)}
                        />
                    </View>
                    <View style={styles.itemForm}>
                        <TextInput
                            style={{paddingLeft:20, width: 230}}
                            value={jumBar}
                            onChangeText={(value)=>setJumlah(value)}
                        />
                    </View>
                    <View style={styles.buttonView}>
                        <TouchableOpacity
                          style={styles.button}
                          onPress={()=>ubahData(keyUbah)}
                        >
                    <Text style={styles.buttonText}>Ubah</Text>
                </TouchableOpacity>
            </View>
            </View>
                </View>
                <View style={styles.buttonView1}>
                        <TouchableOpacity
                            style={styles.button1}
                            onPress={()=>hapusData(keyUbah)}
                        >
                            <Text style={styles.buttonText}>Hapus Data</Text>
                        </TouchableOpacity>
                    </View>
		    </View>
    )
  }

const styles = StyleSheet.create({
	container:{
        flex:1,
    },
    titleBarang:{
        marginTop: 40,
        marginLeft:25,
        marginBottom: 25
    },
    textTitle:{
        fontWeight:'bold',
        fontSize:25,
        color:'#FF5500'
    },
    form:{
        marginTop:20
    },
    itemForm:{
        flexDirection: 'row',
        borderBottomWidth:1,
        borderBottomColor:'#FF5500',
        marginTop: 20,
        marginLeft : 30,
        marginRight : 30,
        marginBottom : 15
    },
    buttonView:{
        marginLeft:30,
        marginRight:30,
        marginTop: 55,
        alignItems:'center'
    },
    buttonView1:{
        marginLeft:30,
        marginRight:30,
        marginTop: 40
    },
    button:{
        backgroundColor:'#FF5500',
        height:35,
        width: 250,
        borderRadius:7,
        alignItems: 'center',
        justifyContent:'center',
    },
    button1:{
        backgroundColor:'#FF5500',
        height:35,
        width: 300,
        borderRadius:7,
        alignItems: 'center',
        justifyContent:'center',
    },
    buttonText:{
        color: 'white',
        fontWeight:'bold',
        fontSize:17,   
    },
})