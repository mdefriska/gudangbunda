import React, {Component} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, Button } from 'react-native';
import { FontAwesome} from '@expo/vector-icons';

export default function About({navigation}) {
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.logo}>
                        <Image
                            source={require('./image/logo.jpg')}
                            style={{height:180, width:180}}
                        />
                    </View>
                    <View style={styles.title}>
                            <Text style={styles.titleText}>Gudang Bunda</Text>
                            <Text style={styles.contentText}>Gudang Bunda merupakan aplikasi yang dapat digunakan untuk
                                                           menginventarisasi barang-barang kebutuhan rumah tangga yang
                                                           habis pakai agar mudah melakukan pencarian barang apa yang
                                                           sudah habis ketika berbelanja.</Text>
                    </View>
                    <View style={{marginTop:100, alignItems:'center'}}>
                        <View style={{flexDirection:'row', marginBottom: 8}}>
                            <FontAwesome name="facebook-square" size={15} color='grey' /> 
                            <Text style={{marginLeft:8, color:'grey'}}>Meriska Defriani</Text>
                        </View>
                        <View style={{flexDirection:'row', marginBottom: 8}}>
                            <FontAwesome name="instagram" size={15} color='grey' /> 
                            <Text style={{marginLeft:8, color:'grey'}}>@meriskadefriani</Text>
                        </View>
                        <View style={{flexDirection:'row'}}> 
                            <Text style={{marginLeft:8, color:'grey'}}>Version 1.0</Text>
                        </View>
                    </View>
                </View>
            </View>
    )
  }

const styles = StyleSheet.create({
	container:{
		flex:1,
        backgroundColor: 'white',
        flexDirection:'column'
	},
	header:{
		backgroundColor:'white',
		marginTop:100,
        flexDirection: 'column',
	},
	logo:{
        alignItems: 'center',
        justifyContent:'center',
	},
    title:{
        justifyContent:'center',
        alignItems: 'center',
        marginTop:50
    },
    titleText:{
        color:'#FF5500', 
        fontSize:25, 
        fontWeight: 'bold',
    },
    contentText:{
        color:'#FF5500', 
        fontSize:16,
        margin: 20,
        textAlign : 'center'
    },
})