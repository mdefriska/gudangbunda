import React, {useState,useEffect} from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList,TouchableOpacity } from 'react-native';
import {dataRef} from './Reference';

export default function Barang({navigation,route}) {
    let [namaBarang, setNamabarang] = useState("");
    let [key, setKey] = useState("");
    let [jumlah, setJumlah] = useState("");
    let [dbarang, setDBarang] = useState([]);

    useEffect(()=> {
        const dataFocus = navigation.addListener('focus', ()=>{
        const listener = dataRef.on('value', (snapshot) => {
          let data = snapshot.val();
          let dbarang = Object.values(data);
          setDBarang(dbarang);
          })})})
    
     const sendData=(item) => {
        navigation.navigate('MyStack',{
            screen:'Ubah',
            params: {key:item.key,
                        namaBarang:item.barang,
                        jumlah:item.jumlah}
        })
      }

    return (
        <View style={styles.container}>
            <View style={styles.titleBarang}>
                <Text style={styles.textTitle}>Daftar Barang</Text>
            </View>
            <View style={styles.containerTitle}>
                <View style={styles.contentTitle}>
                    <Text style={styles.textContentTitle}>Nama Barang</Text>
                </View>
                <View style={styles.contentTitle}>
                    <Text style={styles.textContentTitle}>Jumlah</Text>
                </View>
            </View>
            <View>
            <FlatList
                data={dbarang}
                keyExtractor={(item)=>item.key}
                renderItem={({item})=>{
                    return(
                        <TouchableOpacity
                            onPress={()=>sendData(item)}
                        >   
                            <View style={styles.containerTitle}>
                                <View style = {styles.contentTitle1}>
                                    <Text style = {styles.textContentTitle1}> {item.barang} </Text>
                                </View>
                                <View style = {styles.contentTitle1}>
                                    <Text style = {styles.textContentTitle1}> {item.jumlah}
                                    </Text>
                                </View> 
                            </View>
                    </TouchableOpacity>
                    )
                }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    titleBarang:{
        marginTop: 40,
        marginLeft:25,
        marginBottom: 25
    },
    textTitle:{
        fontWeight:'bold',
        fontSize:25,
        color:'#FF5500'
    },
    containerTitle:{
        flexDirection:'row',
        justifyContent:'space-around',
        backgroundColor: '#FFB894',
        marginLeft: 15,
        marginRight : 15,
        marginBottom: 15,
        height: 30
    },
    contentTitle:{
        marginTop:5,
    },
    contentTitle1:{
        marginTop:5,
        flex:2,
        justifyContent:'center',
        alignItems:'center'
    },
    textContentTitle:{
        fontWeight:'bold',
        color: 'white',
        fontSize:15
    },
    textContentTitle1:{
        fontWeight:'bold',
        fontSize:15
    }
})
